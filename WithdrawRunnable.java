public class WithdrawRunnable implements Runnable
{
    private static final int DELAY = 1;
    private BankAccount account;
    private double amount;
    private int count;

    public WithdrawRunnable(BankAccount account, double amount, int count)
    {
        this.account = account;
        this.amount = amount;
        this.count = count;
    }

    public void run()
    {
        try
        {
            for(int i = 0; i < count; ++i)
            {
                account.withdraw(amount);
                Thread.sleep(DELAY);
            }
        }
        catch(InterruptedException ex)
        {}
    }
}
